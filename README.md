# Memes4U

## Installation

Clone this repository:

```shell
git clone https://gitlab.com/lucaoflaif/memes4u
```

Then move to the downloaded directory and install the required dependencies:

```shell
cd memes4u
sudo pip install -r requirements.txt
```

Almost done, now you have to create thr database and its stuff (such as tables, relationships..):

```shell
python manage.py makemigrations
```
To create our migration (a python file with all the directives), and then:

```shell
python manage.py migrate
```
To apply the directives in the file previously created.

Here we are! You're ready to enjoy a world of memes.

## Getting Started

Now you should be able to run the web server:

```shell
python manage.py runserver
```

By default, the web server runs on `localhost:8000` but you can specify a different port at runtime:

```shell
python manage.py runserver 5000
```

### Useful stuff
All the website's media will be saved in the project directory that'll be 

```
media/SupplierID_OF_SUPPLIER/YEAR/MONTH/DAY/HOURS_MINUTES_SECONDS_MILLISECONDS_FILENAME
```

In example:

```
media/Supplier1/2017/07/05/20_11_26_2702711_dogsimage.jpeg
```

## License

Copyright (c) 2016-2017 **Luca Di Vita**

/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define */
/*global $*/

var ajaxCall = function () {
    "use strict";
    $.ajax({
        url: "/get_img/",
        type: "GET",
        async: true,
        cache: false,
        data: {
            "type_of_request": "new_image",
        },
        success: function (data, status) {
          $(".img_container").css("background-image", "url(" + data.image_url + ")");
          $(".comment_content").text(data.image_comment);
          $(".posted_by_name").text(data.image_supplier_name);
          $(".posted_by_name").attr("href", data.image_link);
          $(".id_number").text(data.image_id);
        }
    });
};

// Gets an initial image.
ajaxCall();

$(".img_container").click(ajaxCall);

$(document).keydown(function (e) {
    "use strict";
    switch (e.which) {
      case 37: // left
      case 39: // right
        ajaxCall();
        break;
    }
});

"""meme4u URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from .settings import DEBUG

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

from random_meme_app import views as random_meme_views

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

#Admin urls
urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

#Login urls
urlpatterns += [
    url(r'accounts/', include('accounts_app.urls')),
]

#Functions urls
urlpatterns += [
    url(r'^home/$', random_meme_views.return_home, name="home"),
    url(r'^get_img/$', random_meme_views.return_random_image, name="return_random_image"),
    url(r'^add/', include('add_app.urls')),
    #url(r'accounts/', include('accounts_app.urls')),

]

if DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,
                                                                                            document_root=settings.
                                                                                            MEDIA_ROOT)

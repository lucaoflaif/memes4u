from django.contrib import admin

from .models import Profile
from django.utils.translation import ugettext, ugettext_lazy as _


# Register your models here.

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):

    fieldsets = (
            (_('Profile'), {'fields': ('user','link')}),
        )
    add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('username', 'password1', 'password2'),
            }),
        )
    list_display = ('user', 'link')
    search_fields = ['user__username', 'link'] 


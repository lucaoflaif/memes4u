from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required()
def return_user_profile(request):

    context = {'current_user': request.user}

    return render(request, 'accounts/profile.html', context)

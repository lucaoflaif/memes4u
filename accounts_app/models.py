from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver




# Create your models here.

class Profile(models.Model):
    def __str__(self):
        return '{} profile'.format(self.user.username)
    
    user = models.OneToOneField(User)
    link = models.URLField(null=True, blank=True)

#Create an instance of Profile model when User is created
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

#Save an instance of Profile model when User is saved
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


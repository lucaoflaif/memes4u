from django.contrib import admin

# Register your models here.

from .models import Meme_image
from django.utils.translation import ugettext, ugettext_lazy as _


# Register your models here.

@admin.register(Meme_image)
class MemeImage(admin.ModelAdmin):

    fieldsets = (
            (_('Meme images'), {'fields': ('user','image','comment')}),
        )
    add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('user', 'image','comment'),
            }),
        )
    list_display = ('user','image','upload_date','comment')
    search_fields = ['user__username', 'upload_date']


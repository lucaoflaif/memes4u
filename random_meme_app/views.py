from django.shortcuts import render
from . import models
from django.http import JsonResponse

import random

# Create your views here.

def return_random_image(request):
    if request.is_ajax() and request.method == "GET":

        if request.GET.get("type_of_request", False):
            if request.GET["type_of_request"] == "new_image":
                #A new image requested

                kwargs= {} #For programmatically lookup, kwargs[name_of_lookup(s)] = value

                queryset = models.Meme_image.objects.filter(**kwargs)
                meme = queryset[random.randint(0, queryset.count() -1)]

                data_dict = {"image_url": meme.image.url,
                                "image_id": meme.id,
                                "image_supplier_name": ' '.join((meme.user.first_name, meme.user.last_name)),
                                "image_posted_date": meme.upload_date,
                                "image_comment": meme.comment,
                                "image_link": meme.user.profile.link
                            }

                return JsonResponse(data_dict)


def return_home(request):
    return render(request, 'home.html')

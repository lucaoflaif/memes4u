from django.apps import AppConfig


class RandomMemeAppConfig(AppConfig):
    name = 'random_meme_app'

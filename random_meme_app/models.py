from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

# Create your models here.

User = get_user_model()


def meme_destination(istance, filename):
    return 'user{}/{}/{}/{}/{}'.format(istance.user.id,
                                       timezone.localtime(timezone.now()).strftime('%Y'),
                                       timezone.localtime(timezone.now()).strftime('%m'),
                                       timezone.localtime(timezone.now()).strftime('%d'),
                                       timezone.localtime(timezone.now()).strftime('%H_%M_%S_%f_')
                                       + filename)


class Meme_image(models.Model):
    user = models.ForeignKey(User)
    image = models.ImageField(upload_to=meme_destination)
    upload_date = models.DateTimeField(default=timezone.now)
    comment = models.CharField(max_length=65, null=True)



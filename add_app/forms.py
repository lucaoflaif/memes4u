from django.forms import ModelForm
from random_meme_app.models import Meme_image


class InsertMemeImageForm(ModelForm):
    class Meta:
        model = Meme_image
        fields = '__all__'
        exclude = ['user', 'upload_date']

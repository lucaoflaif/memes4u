from django.conf.urls import url
from . import views as add_app_views


urlpatterns = [
    # Examples:
    # url(r'^$', 'ecommerce.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url('^', include('django.contrib.auth.urls')),

    url(r'meme_image/$', add_app_views.add_meme_image, name='add_meme_image'),
    
    
]
from django.shortcuts import render, redirect
from . import forms
from django.contrib.auth.decorators import login_required
from django.http import Http404

# Create your views here.
@login_required()
def add_meme_image(request):

    if request.method == "GET":
        template_name = "add_app/add_meme_image.html"
 
        context = {
            'form': forms.InsertMemeImageForm
        }

        return render(request, template_name, context)

    elif request.method == "POST":
        form = forms.InsertMemeImageForm(request.POST, request.FILES)
        if form.is_valid():
            new_meme_image = form.save(commit=False)  # It returns the istance without saving it
            new_meme_image.user = request.user  # I do some stuff with the instance
            new_meme_image.save()  # Now it is saved in the DB

            return redirect("home")
        else:
            raise Http404
        pass
